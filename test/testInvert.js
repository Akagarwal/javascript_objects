const InvertFile = require('../invert')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham', tags: {talent: 'millionaire', series: 'dc'} };

console.log(InvertFile.invert(testObject));
console.log(InvertFile.invert());