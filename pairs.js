function pairs(obj){
    if(!obj){
        return [];
    }
    let result = [];
    for (let key in obj){
        let temp = [key, obj[key]];
        result.push(temp);
    };
    return result;
}
module.exports = {pairs};










// const pairs = function (obj) {
//     if (!obj) {
//         return [];
//     }
//     let listOfKeyValuePairs = [];
//     for (let key in obj) {
//         let pair = [key, obj[key]];
//         listOfKeyValuePairs.push(pair);
//     }
//     return listOfKeyValuePairs;
// }
// module.exports = pairs;