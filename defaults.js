function defaults(obj, defaultProps){
    if(!obj || !defaultProps || Array.isArray(obj) || Array.isArray(defaultProps)){
        return {};
    }
    let returnObject = defaultProps;
    for(let i in obj){
        returnObject[i] = obj[i]
    }
    return returnObject;
}
module.exports = {defaults};