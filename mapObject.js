function mapObject(obj, cb) 
{
    let returnObject = {}
    if (!obj || Array.isArray(obj) || !cb)
    {
        return returnObject
    }
    for (let key in obj)
    {
        returnObject[key] = cb(key, obj[key]);
    }
    return returnObject    
}

const modifyingKeyValue = (key,value) => 
{
    if (!key || !value)
    {
        return ;
    }
    let returnValue = ("My "+key+" is "+value)
    return returnValue;
}

module.exports = {mapObject,modifyingKeyValue}
