const defaultsFile = require('../defaults')

let coffee = {type: "mocha"};
let defaultData = {type: "cappuccino", topping: "heart"}

console.log(defaultsFile.defaults(coffee,defaultData))//normal
console.log(defaultsFile.defaults(coffee))//no default
console.log(defaultsFile.defaults(defaultData))//no main obj
console.log(defaultsFile.defaults())//empty
