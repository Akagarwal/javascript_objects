function invert(obj) {
    let returnObject = {};
    if (!obj || Array.isArray(obj)){
        return {};
    };
    for (let i in obj){
        let Vals = obj[i];
        if(typeof Vals === 'object'){
            Vals = JSON.stringify(Vals)
        };
        returnObject[Vals] = i;
    };
    return returnObject
    
}

module.exports = {invert};